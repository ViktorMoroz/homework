function openNav(event) {
  const icon = event.target.dataset.icon;
  const newIcon = event.target.innerHTML;
  event.target.innerHTML = icon;
  event.target.dataset.icon = newIcon;
  document.querySelector(".nav__ul").classList.toggle("nav__ul--active");
}
document
  .querySelector(".material-symbols-outlined")
  .addEventListener("click", openNav);