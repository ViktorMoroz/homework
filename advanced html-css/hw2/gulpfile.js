const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const browserSync = require("browser-sync");
const fileinclude = require('gulp-file-include'); 
function minifyImg () {
	return gulp.src('src/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
};
function doClean() {
    return gulp.src('dist', {read: false})
        .pipe(clean());
};
function buildStyles() {
    return gulp.src('src/scss/*.scss')
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(autoprefixer({cascade: false}))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(concat('all.css'))
      .pipe(gulp.dest('dist/css'));
  };
function buildJS() {
    return gulp.src('./src/js/*.js')
      .pipe(concat('all.js'))
      .pipe(uglify())
      .pipe(gulp.dest('./dist/js/'));
  };
function buildHtml(){
  return gulp.src('src/index.html')
  .pipe(fileinclude({
    prefix: '@@',
    basepath: '@file'
  }))
  .pipe(gulp.dest('dist/'))
}
function browserSyncServer(cb){
  browserSync.init({
    server: {
      baseDir: './dist'
    }
  })
  cb();
}
function browsersyncReload(cb){
  browserSync.reload();
  cb();
}
function watchStylesJs(){
  gulp.watch(['src/scss/**/*.scss', 'src/js/**/*.js'], gulp.series(buildStyles, buildJS, browsersyncReload));
}
  exports.buildStyles = buildStyles;
  exports.buildJS = buildJS;
  exports.doClean = doClean;
  exports.buildHtml = buildHtml;
  exports.minifyImg = minifyImg;
  gulp.task('build', gulp.series(doClean, buildStyles, buildJS, buildHtml, minifyImg));
  gulp.task('buildFast', gulp.series(buildStyles, buildJS, buildHtml));
  exports.dev = gulp.series(buildStyles, buildJS, browserSyncServer, watchStylesJs);