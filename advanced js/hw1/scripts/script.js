/* Теоретичне питання
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

    Прототипне наслідування в JavaScript - це механізм, за допомогою якого об’єкти можуть наслідувати властивості
    один від одного. Кожен об’єкт має прототип, який може бути іншим об’єктом або null. Якщо властивість не знайдена
    в об’єкті, JavaScript автоматично перевіряє його прототип. Цей процес продовжується до тих пір, поки властивість
    не буде знайдена або не досягне кінця ланцюжка прототипів. Якщо властивість не знайдена ні в одному з об’єктів у
    ланцюжку прототипів, повертається undefined.

Для чого потрібно викликати super() у конструкторі класу-нащадка?

    super() використовується для виклику конструктора батьківського класу з конструктора класу-нащадка.
    Це дозволяє успадкованому класу отримати доступ до властивостей та методів батьківського класу.

*/
class Employee {
    constructor (name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name (){
        return this._name;
    }
    set name (name){
        this._name = name;
    }
    get age (){
        return this._age;
    }
    set age (age){
        this._age = age;
    }
    get salary (){
        return this._salary;
    }
    set salary (salary){
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary*3;
    }
}

const Vasya = new Programmer ("Vasya", 25, 30000, "javascript");
const Katya = new Programmer ("Katya", 27, 45678, "javascript, java, C++");
const Misha = new Programmer ("Misha", 64, 19999, "Pascal, Delphi");
console.log(Vasya, Katya, Misha);