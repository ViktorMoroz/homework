function remove(event) {
  fetch(
    `https://ajax.test-danit.com/api/json/posts/${event.target.dataset.card}`,
    {
      method: "DELETE",
    }
  ).then(() => {
    document.querySelector(`#card${event.target.dataset.card}`).remove();
  });
}
class Card {
  constructor(heading, text, name, eMail, id) {
    this.heading = heading;
    this.text = text;
    this.name = name;
    this.eMail = eMail;
    this.id = id;
  }
}
let users;
fetch("https://ajax.test-danit.com/api/json/users")
  .then((response) => response.json())
  .then((result) => {
    users = result;
  });
fetch("https://ajax.test-danit.com/api/json/posts")
  .then((response) => response.json())
  .then((result) => {
    result.forEach((element) => {
      const article = document.createElement("article");
      const card = new Card(
        element.title,
        element.body,
        users[element.userId - 1].name,
        users[element.userId - 1].email,
        element.id
      );
      article.innerHTML = `
    <h2> ${card.heading}</h2>
    <span>${card.name}</span>
    <span>${card.eMail}</span>
    <p>${card.text}</p>
    <button id="button${card.id}" data-card="${card.id}">Delete</button>
  `;
      document.body.append(article);
      document
        .querySelector(`#button${card.id}`)
        .addEventListener("click", remove);
      article.setAttribute("id", `card${card.id}`);
    });
  });
