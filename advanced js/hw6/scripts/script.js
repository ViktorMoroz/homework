/*
Теоретичне питання
  Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

    Асинхронність в javascript означає, що деякі операції можуть виконуватися паралельно з
  основним потоком виконання коду, не блокуючи його. Це корисно для операцій, які потребують
  часу або доступу до зовнішніх ресурсів, наприклад запитів до сервера, читання файлів або
  роботи з веб-камерою. Асинхронність дозволяє сторінці залишатися відгуковою та ефективною.
*/
async function searchIp() {
  document.querySelector("#result").innerHTML = "<p>Зачекайте...</p>";
  const ip = await (await fetch("https://api.ipify.org/?format=json")).json();
  const address = await (
    await fetch(
      `http://ip-api.com/json/${ip.ip}?fields=continent,country,region,city,district`
    )
  ).json();
  document.querySelector("#result").innerHTML = `
  <p>Континент: ${address.continent}</p>
  <p>Країна: ${address.country}</p>
  <p>Регіон: ${address.region}</p>
  <p>Місто: ${address.city}</p>
  <p>Район: ${address.district}</p>
  `;
}
document.querySelector("button").addEventListener("click", searchIp);
