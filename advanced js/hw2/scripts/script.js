/*

ТЕОРЕТИЧНЕ ПИТАННЯ:
  
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
    
  Якщо в коді є функція, яка може зламатись під час виконання, наприклад внаслідок:
    - неправильного вводу даних користувачем,
    - недоступності сервера;
  то можна використати try...catch для обробки цих помилок.

*/
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

let ul = document.createElement("ul");
document.querySelector("#root").append(ul);

for (let i = 0; i < books.length; i++) {
  try { 
    if (!books[i].price){
      throw new Error(`У книги під номером ${i+1} відсутня властивість "price"`);
    }
    if (!books[i].name){
        throw new Error(`У книги під номером ${i+1} відсутня властивість "name"`);
    }
    if (!books[i].author){
        throw new Error(`У книги під номером ${i+1} відсутня властивість "author"`);
    }
    let li = document.createElement("li");
    li.innerHTML = books[i].name + ", автор: " + books[i].author + ", ціна: " + books[i].price;
    ul.append(li);
  } catch (e) {
    console.log(e.message);
  }
}