/*
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

  AJAX дозволяє веб-додаткам здійснювати швидкі та поступові оновлення інтерфейсу 
  користувача без перезавантаження всієї сторінки браузера. Це робить додаток
  швидшим і більш відгуковим на дії користувача.

  AJAX корисний при розробці Javascript, тому що ви можете:
    - Читати дані з веб-сервера - після завантаження сторінки
    - Оновлювати веб-сторінку без перезавантаження сторінки
    - Надсилати дані на веб-сервер - у фоновому режимі
*/
fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((starwars) => {
    starwars.forEach(({ characters, episodeId, name, openingCrawl }) => {
      const ul = document.createElement("ul");
      ul.innerHTML = `
    <li>Star Wars: ${name}</li>
    <li>Episode #${episodeId}</li>
    <li>${openingCrawl}</li>
    <li class="chars${episodeId}">Основні персонажі: </li>  
    `;
      document.body.append(ul);
      const chars = document.querySelector(`.chars${episodeId}`);
      const ol = document.createElement("ol");
      chars.append(ol);
      characters.forEach((character) => {
        fetch(`${character}`)
          .then((response) => response.json())
          .then(({ name }) => {
            ol.innerHTML += `<li>${name}</li>`;
          });
      });
    });
  });
