/*
Чому для роботи з input не рекомендується використовувати клавіатуру?
	Можуть виникати проблеми, коли користувачі використовують різні розкладки клавіатури,
	також користувачі можуть допустити помилку при введені даних, і в деяких випадках це
	може призвести до невірного результату. Також, в залежності від задачі і потреб користувача,
	інші методи вводу можуть бути кращими за клавіатуру.
*/
let blueKey;
function keyPressed(event) {
  if (blueKey != undefined) {
    blueKey.classList.remove("btn-key");
  }
  blueKey = document.querySelector(`.btn[data-key="${event.code}`);
  if (blueKey != null) {
    blueKey.classList.add("btn-key");
  }
}
document.addEventListener("keydown", keyPressed);