/*
Теоретичні питання
    1. Опишіть своїми словами, що таке метод об'єкту

        Це дія яка може бути виконана всередині об'єкту, це наче функція, але зі своїм специфічним синтаксисом.

    2. Який тип даних може мати значення властивості об'єкта?
        
        Будь-який.

    3. Об'єкт це посилальний тип даних. Що означає це поняття?

        Це означає, що його ім'я дає посилання до комірок пам'яті, в яких знаходяться властивості цього об'єкту.

*/

function createNewUser() {
  const fName = prompt("Enter your First Name, please");
  const lName = prompt("Enter your Last Name, please");

  return (newUser = {
    firstName: fName,
    lastName: lName,
    getLogin: function () {
      return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
    },
  });
}

let newUser;
createNewUser();
console.log(newUser.getLogin());