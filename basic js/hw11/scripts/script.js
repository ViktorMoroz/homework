document.querySelector(".password-form").onclick = function (event) {
  if (event.target.classList.contains("fas")) {
    if (event.target.classList.contains("fa-eye-slash")) {
      event.target.classList.remove("fa-eye-slash");
      event.target.previousElementSibling.type = "password";
    } else {
      event.target.classList.add("fa-eye-slash");
      event.target.previousElementSibling.type = "text";
    }
  }
};
document.body.querySelector(".btn").onclick = function (event) {
  if (
    document.querySelector("#password-1").value ===
    document.querySelector("#password-2").value
  ) {
    document.querySelector("p").textContent = "You are welcome";
  } else {
    document.querySelector("p").textContent =
      "Потрібно ввести однакові значення";
  }
};
