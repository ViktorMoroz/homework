/*

Теоретичні питання

    1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

        Екранування використовується, щоб написати символ, який зарезервований мовою програмування,
        щоб js інтерпретував його саме як символ, а не як частину коду, або написати спецсимвол, який по іншому не ввести.

    2. Які засоби оголошення функцій ви знаєте?

        Function Declaration, Function Expression, Named Function Expression, Arrow Function
    
    3. Що таке hoisting, як він працює для змінних та функцій?

        Hoisting дає можливість отримати доступ до змінних та функцій до того, як вони були оголошені.

*/

function createNewUser() {
    const fName = prompt("Enter your First Name, please");
    const lName = prompt("Enter your Last Name, please");
    const bDay = prompt("Enter your Birthday, please\ndd.mm.yyyy");

    return (newUser = {
        firstName: fName,
        lastName: lName,
        birthday: bDay,
        getLogin: function () {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge: function () {
            const day = bDay.slice(0, 2);
            const month = bDay.slice(3, 5);
            const year = bDay.slice(6);
            const birthday = new Date(`${year}-${month}-${day}`);
            const today = new Date();
            return Math.floor(`${today - birthday}` / 31556952000);
        },
        getPassword: function () {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${bDay.slice(6)}`;
        }
    });
}

let newUser;
createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());




