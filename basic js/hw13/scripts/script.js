/*
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

  setTimeout() виконається 1 раз після заданого інтервалу, а setInterval() буде повторювати своє виконання поки не закриєш сторінку, або не зупиниш вручну.

2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

  Ні, вона не спрацює миттєво, а виконається настільки швидко, наскільки це можливо. Бо планувальник викличе функцію лише після того, як завершиться виконання інших завдань.

3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

  Це важкливо, бо функція продовжить виконуватися, і це буде забирати ресурси комп'ютера, а в свою чергу це може знизити продуктивність веб-сторінки.
*/
let startInterval;
const images = Array.prototype.slice.call(
  document.querySelectorAll(".image-to-show")
);
const startButton = document.body.querySelector("#start");
const stopButton = document.body.querySelector("#stop");
function showImages() {
  const indexOfImageActive = images.indexOf(
    document.querySelector(".image-active")
  );
  const imageActive = images[indexOfImageActive];
  const nextImageActive = images[indexOfImageActive + 1];
  if (indexOfImageActive < images.length - 1) {
    imageActive.classList.remove("image-active");
    nextImageActive.classList.add("image-active");
  } else {
    imageActive.classList.remove("image-active");
    document.querySelector(".image-to-show").classList.add("image-active");
  }
}
document
  .querySelector(".buttons-wrapper")
  .addEventListener("click", function (event) {
    if (event.target === startButton) {
      if (!startInterval) {
        startButton.textContent = "Відновити показ";
        stopButton.classList.remove("invisible-button");
        startButton.setAttribute("disabled", "");
        stopButton.removeAttribute("disabled");
        startInterval = setInterval(showImages, 3000);
      }
    }
    if (event.target === stopButton) {
      clearInterval(startInterval);
      startInterval = null;
      startButton.removeAttribute("disabled");
      stopButton.setAttribute("disabled", "");
    }
  });
