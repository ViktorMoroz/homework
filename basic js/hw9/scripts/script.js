/*
1. Опишіть, як можна створити новий HTML тег на сторінці.

    Треба створити новий елемент за допогою методу:
        document.createElement
    Потім додати його на сторінку за допомогою одного з методів:
        node.append
        node.prepend
        node.before
        node.after
        node.replaceWit

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

    Перший параметр означає "where", тобто куди буде вставлений html рядок, наприклад:
        element.insertAdjacentHTML('beforeend', '<p>Вставив параграф в кінець element</p>')

3. Як можна видалити елемент зі сторінки?

    Потрібно використати метод node.remove

*/
function createList(array, parent = document.body) {
  const uList = document.createElement("ul");
  parent.append(uList);
  uList.setAttribute("id", "uList");
  for (let index = 0; index < array.length; index++) {
    const ul = document.getElementById("uList");
    const li = document.createElement("li");
    li.append(document.createTextNode(`${array[index]}`));
    ul.append(li);
  }
}
createList([1, "word", 2, "one more", "maybe this one too"], document.body.querySelector("div"));