function onClick(event) {
  const tabsTitle = document.querySelectorAll(".tabs-title");
  for (element of tabsTitle) {
    element.classList.remove("active");
  }
  event.target.classList.add("active");
  const arrayTabs = Array.prototype.slice.call(tabsTitle);
  const indexOfItem = arrayTabs.indexOf(event.target);
  for (element of document.querySelectorAll(".tabs-content-item")) {
    element.classList.remove("tabs-content-item-active");
  }
  document
    .querySelectorAll(".tabs-content-item")
    [indexOfItem].classList.add("tabs-content-item-active");
}
document.querySelector(".tabs").addEventListener("click", onClick);
