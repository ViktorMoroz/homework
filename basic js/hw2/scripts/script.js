/*
    Теоретичні питання

    1. Які існують типи даних у Javascript?

        Типи даних у Javascript:
            1. String
            2. Number
            3. Bigint
            4. Boolean
            5. Undefined
            6. Null
            7. Symbol
            8. Object

    2. У чому різниця між == і ===?

        == порівнює два значення.
        === порівнює два значення та типи цих значень.


    3. Що таке оператор?

        Оператор - спеціальний знак, за допомогою якого відбувається маніпуляція над операндами.

*/
let areYouSure = false;
let yourName = prompt("Enter your Name");
let yourAge = prompt("Enter your age");
if(yourName === null){
    yourName = " ";
}
if(yourAge === null){
    yourAge = " ";
}
while (yourName.trim().length < 1 || yourAge < 1 || isNaN(yourAge) === true) {
    alert("Not Valid!");
    yourName = prompt("Enter your Name", yourName);
    if(yourName === null){
        yourName = " ";
    }
    yourAge = prompt("Enter your age", yourAge);
    if(yourAge === null){
        yourAge = " ";
    }
}
if (yourAge >= 22) {
    areYouSure = true;
} else if (yourAge >= 18) {
    areYouSure = confirm("Are you sure you want to continue?");
}
if (areYouSure) {
    alert("Welcome, " + yourName);
} else {
    alert("You are not allowed to visit this website");
}