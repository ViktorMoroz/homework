function clickService(event) {
  if (event.target.classList.contains("service-list-item")) {
    const listItem = document.querySelectorAll(".service-list-item");
    for (element of listItem) {
      element.classList.remove("service-list-item-active");
    }
    event.target.classList.add("service-list-item-active");
    const arrayList = Array.prototype.slice.call(listItem);
    const indexOfItem = arrayList.indexOf(event.target);
    for (element of document.querySelectorAll(".service-list-content")) {
      element.classList.remove("service-list-content-active");
    }
    document
      .querySelectorAll(".service-list-content")
      [indexOfItem].classList.add("service-list-content-active");
  }
}
function clickWork(event) {
  if (event.target.classList.contains("work-list-item")) {
    const listItem = document.querySelectorAll(".work-list-item");
    for (element of listItem) {
      element.classList.remove("work-list-item-active");
    }
    event.target.classList.add("work-list-item-active");
    for (element of document.querySelectorAll(".work-grid-item")) {
      element.classList.remove("work-grid-item-active");
    }
    for (element of document.querySelectorAll(
      `.${event.target.dataset.show}`
    )) {
      element.classList.add("work-grid-item-active");
    }
    if (event.target.classList.contains("work-list-item-all")) {
      document.querySelector(".button-load").style.display = "inline";
    } else {
      document.querySelector(".button-load").style.display = "none";
    }
  }
}
function clickLoad(event) {
  for (element of document.querySelectorAll(".work-grid-item-to-load")) {
    element.classList.add("work-grid-item-active");
  }
  event.currentTarget.style.display = "none";
}
function clickClients(event) {
  let prevQoute = document.querySelector(".active");
  let prevImg = document.querySelector(".feedback-clients-img-active");
  let nextQuote;
  let nextImg;
  if (event.target.classList.contains("feedback-clients-img")) {
    prevImg.classList.remove("feedback-clients-img-active");
    nextImg = event.target;
    nextQuote = document.querySelector(`#${event.target.dataset.name}`);
    return runCarousele(nextImg, prevImg, nextQuote, prevQoute);
  } else if (event.target.classList.contains("feedback-button")) {
    if (event.target.dataset.name == "white") {
      nextQuote = prevQoute.previousElementSibling;
      nextImg = prevImg.previousElementSibling;
    } else {
      nextQuote = prevQoute.nextElementSibling;
      nextImg = prevImg.nextElementSibling;
    }
    if (nextQuote !== null) {
      return runCarousele(nextImg, prevImg, nextQuote, prevQoute);
    } else {
      nextQuote = document.querySelector(`#${event.target.dataset.name}`);
      nextImg = document.querySelector(`#${event.target.dataset.img}`);
      return runCarousele(nextImg, prevImg, nextQuote, prevQoute);
    }
  }
}
function runCarousele(nextImg, prevImg, nextQuote, prevQoute) {
  prevImg.classList.remove("feedback-clients-img-active");
  nextImg.classList.add("feedback-clients-img-active");
  prevQoute.classList.add("hidden");
  prevQoute.classList.remove("active");
  nextQuote.classList.add("active");
  nextQuote.classList.remove("hidden");
}
document.querySelector(".service-list").addEventListener("click", clickService);
document.querySelector(".work-list").addEventListener("click", clickWork);
document.querySelector(".button-load").addEventListener("click", clickLoad);
document
  .querySelector(".feedback-clients")
  .addEventListener("click", clickClients);