/*
1. Опишіть своїми словами що таке Document Object Model (DOM)

    Це модель для представлення документу у вигляді об'єкту

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerText - дозволяє задавати або отримувати тільки текстовий контент елементу
    innerHTML - Дозволяє задавати або отримувати html розмітку елементу, а не тільки текст

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

    Щоб звернутись елементу сторінки, треба застосувати один із методів до об'єкту document:
        querySelector
        querySelectorAll
        getElementById
        getElementByName
        getElementByTagName
        getElementByClassName
    На мою думку, при валідному html коді, найкращий спосіб звернутись до елементу - getElementById,
    тому що id - унікальний, і я буду певен, навіть, якщо у майбутньому додатуть або видалять якісь
    елементи до нашої сторінки, що я звернусь саме до того елементу, який мені потрібен.
*/

const allParagraphs = document.querySelectorAll("p");
for (element of allParagraphs) {
  element.style.backgroundColor = "#ff0000";
}

console.log(document.getElementById("optionsList"));
console.log(document.getElementById("optionsList").parentElement);
for (element of document.getElementById("optionsList").childNodes) {
  console.log("nodeName: " + element.nodeName);
  console.log("nodeType: " + element.nodeType);
}

console.log(document.getElementsByClassName("testParagraph")); // Пуста колекція
/*
Неможливо встановити елементу з класом testParagraph якийсь контент, бо такого елемента
не існує, але цей код виглядав би приблизно так (якщо елемент один): 
    document.getElementsByClassName("testParagraph")[0].textContent = "This is a paragraph";
*/

const headerChildren =
  document.getElementsByClassName("main-header")[0].children;
for (element of headerChildren) {
  console.log(element);
  element.className = "nav-item";
}

const sectionTitle = document.getElementsByClassName("section-title");
while (sectionTitle.length != 0) {
  sectionTitle[0].classList.remove("section-title");
}
