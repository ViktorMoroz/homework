/*
Теоретичні питання
    1. Опишіть своїми словами як працює метод forEach.

        Цей метод виконає функцію, яку ми вкажемо, для кожного елементу масиву

    2. Як очистити масив?

        Використати array.splice() наступним чином:

            array = array.splice();

    3. Як можна перевірити, що та чи інша змінна є масивом?

        Використати метод array.isArray()
*/
let newArray;
function filterBy(array, typeOfData) {
    return newArray = array.filter(element => (typeof element !== typeOfData || element === null) && (typeOfData !== "null" || element !== null));
}
console.log(filterBy(['hello', 'world', 23, '23', null, undefined, {}, { id: 123, points: 14 }, [1, 2, 3, 4]], "object"));