import {BrowserRouter, Routes, Route} from "react-router-dom";
import Layout from "../Layouts/Layout";
import Basket from "../Pages/Basket";
import Favourites from "../Pages/Favourites";
import Home from "../Pages/Home";
import PageNotFound from "../Pages/PageNotFound";
export default function Router() {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route index element={<Home />}></Route>
                        <Route path="Basket/" element={<Basket />}></Route>
                        <Route path="Favourites/" element={<Favourites />}></Route>
                        <Route path="*" element={<PageNotFound />}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    )
}
