import React from "react";
import ButtonToShowModal from "../ButtonToShowModal";
import {CloseIcon} from "../Icons";
import PropTypes from 'prop-types';
export default function ShoppingCard (props){
   
     function buttonOnClick() {
        this.setState({ showModal: true });
      };
      const shouldRender = ()=>{props.handleChildClick();
          }   
    return (<div className="shopping-card"><p>{props.card.name}</p>
    <ButtonToShowModal
          text={<CloseIcon fill="black"/>}
          backgroundColor=""
          onClick={buttonOnClick}
          className="delete-from-basket"
          modal={{
            headerText: "Видалити товар з кошика?",
            closeButton: true,
            text: "Натисніть Так, щоб видалити товар з кошика",
            actions: [
              <button className="close-button" key={1} onClick={()=>{
                  localStorage.shoppingCount=Number(localStorage.shoppingCount)-1;
                  if(localStorage.shoppingCount>0){
                  document.querySelector("#shopping-number").innerText=localStorage.shoppingCount;
                  } else {
                    document.querySelector("#shopping-number").innerText="";
                  }
                  const shoppingList = localStorage.shoppingList.split(' ');
                  const index = shoppingList.indexOf(props.card.id);
                  shoppingList.splice(index, 1);
                  localStorage.shoppingList= shoppingList.join(" ")
                  shouldRender();
                }
              }>
                Так
              </button>,
              <button className="close-button" key={2}>
                Ні
              </button>,
            ],
          }}
        ></ButtonToShowModal>
    
    </div>)
}

ShoppingCard.propTypes = {
  card: PropTypes.object,
  handleChildClick: PropTypes.func
}
