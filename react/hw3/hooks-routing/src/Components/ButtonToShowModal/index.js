import React from "react";
import Modal from "../Modal";
import PropTypes from 'prop-types';
class ButtonToShowModal extends React.Component {
    constructor(){
        super();
        this.state = {
          showModal: false,
        }
        this.closeModal = this.closeModal.bind(this)
    }
    closeModal(){
        this.setState({ showModal: false })
    }
    render (){
        const click = this.props.onClick.bind(this);
        
        return(
            <div>
            <button className={this.props.className} onClick={()=>click()}>{this.props.text}</button>
            {this.state.showModal && (<Modal modal={this.props.modal} closeModal={this.closeModal}/>)}
            </div>
        )
    }
}

export default ButtonToShowModal;
ButtonToShowModal.propTypes ={
    className: PropTypes.string,
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,  
    ]),
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func,
    modal: PropTypes.object,
};
ButtonToShowModal.defaultProps = {
        text:"Example",
        backgroundColor:"grey",
        onClick: ()=>{console.log("example")},
        className: "example",
        modal: {
            headerText: "Heading",
            closeButton: true,
            text: "Text",
            actions: [
              <button className="close-button" key={1}>
                Some Action Button
              </button>,
            ],
}
}