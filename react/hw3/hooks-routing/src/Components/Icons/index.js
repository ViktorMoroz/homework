import React from "react";
import PropTypes from 'prop-types';
export function Basket() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="25"
      height="25"
      fill="none"
      stroke="#000"
      viewBox="0 0 24 24"
    >
      <path
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M20 10l-1.485 7.428c-.184.916-.275 1.374-.515 1.717a2 2 0 01-.839.688c-.383.167-.85.167-1.784.167H8.623c-.934 0-1.401 0-1.784-.167a2 2 0 01-.84-.688c-.239-.343-.33-.801-.513-1.717L4 10m16 0h-2m2 0h1M4 10H3m1 0h2m0 0h12M6 10l3-6m9 6l-3-6m-6 9v3m3-3v3m3-3v3"
      ></path>
    </svg>
  );
}
export function Star({fill, onClick}) {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="20"
        height="20"
        fill={fill}
        stroke="#000"
        viewBox="0 0 47.94 47.94"
        onClick={onClick}
      >
        <path
          d="M26.285 2.486l5.407 10.956a2.58 2.58 0 001.944 1.412l12.091 1.757c2.118.308 2.963 2.91 1.431 4.403l-8.749 8.528a2.582 2.582 0 00-.742 2.285l2.065 12.042c.362 2.109-1.852 3.717-3.746 2.722l-10.814-5.685a2.585 2.585 0 00-2.403 0l-10.814 5.685c-1.894.996-4.108-.613-3.746-2.722l2.065-12.042a2.582 2.582 0 00-.742-2.285L.783 21.014c-1.532-1.494-.687-4.096 1.431-4.403l12.091-1.757a2.58 2.58 0 001.944-1.412l5.407-10.956c.946-1.919 3.682-1.919 4.629 0z"
          className="star-icon"
        ></path>
      </svg>
    );
  }

export function CloseIcon({fill}) {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="20"
        height="20"
        fillRule="nonzero"
        viewBox="0 0 256 256"
        id="close-icon"
      >
        <path
          fill={fill}
          strokeMiterlimit="10"
          d="M7.719 6.281L6.28 7.72 23.563 25 6.28 42.281 7.72 43.72 25 26.437 42.281 43.72l1.438-1.438L26.437 25 43.72 7.719 42.28 6.28 25 23.563z"
          fontFamily="none"
          fontSize="none"
          fontWeight="none"
          textAnchor="none"
          transform="scale(5.12)"
        ></path>
      </svg>
    );
  }  
Star.propTypes = {
  fill: PropTypes.string,
  onCLick : PropTypes.func
}
CloseIcon.propTypes = {
  fill: PropTypes.string
}