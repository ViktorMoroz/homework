import React, {useState, useEffect} from 'react';
import CardList from '../Components/CardList';
export default function Favourites () {
    const [state, setState] = useState({
        listOfGoods: false,
        favOnly: false,
    });
    const [shouldRender, setShouldRender] = useState(false);

    const handleChildClick = () => {
      setShouldRender(!shouldRender);
    };
    useEffect(()=>{ 
    fetch("../listofgoods.json")
    .then((result) => result.json())
    .then((result) => setState({ listOfGoods : result, favOnly : true }))
    }, [])
    
    return (
        <>
        <main>
          {state.listOfGoods && (<CardList listOfGoods = {state.listOfGoods} favOnly = {state.favOnly} onChildClick={handleChildClick}></CardList>)}
        </main>
        </>
    )
}
