import React from "react";
import CardList from "../Components/CardList"
export default class Home extends React.Component {
    constructor() {
      super();
      this.state = {
        listOfGoods : false,
        favOnly: false
      }
    }
    componentDidMount() {
      fetch("listofgoods.json")
        .then((result) => result.json())
        .then((result) => this.setState({ listOfGoods : result }))
    }
    buttonOnClick() {
      this.setState({ showModal: true });
    }
    render() {
      return (
        <div>
        <main>
          {this.state.listOfGoods && (<CardList listOfGoods = {this.state.listOfGoods} favOnly = {this.state.favOnly} ></CardList>)}
        </main>
        </div>
      );
    }
  }