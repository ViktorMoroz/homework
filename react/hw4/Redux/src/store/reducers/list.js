const initialState = {
    listOfGoods : [],
    isLoading: false,
    error: null
}

const listOfGoods = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_DATA_START':
        return { ...state, isLoading: true, error: null };
      case 'FETCH_DATA_SUCCESS':
        return { ...state, listOfGoods: action.payload, isLoading: false };
      case 'FETCH_DATA_FAILURE':
        return { ...state, error: action.payload, isLoading: false };
      default:
        return state;
    }
  };

export default listOfGoods;

export const fetchList = (fetchadress) => {
    return async dispatch => {
      dispatch({ type: 'FETCH_DATA_START' });
      // fetch("listofgoods.json")
      //   .then(response => response.json())
      //   .then(data => {
      //     dispatch({ type: 'FETCH_DATA_SUCCESS', payload: data });
      //   })
      //   .catch(error => {
      //     dispatch({ type: 'FETCH_DATA_FAILURE', payload: error });
      //   });
      try {
        console.log("trying to fetch...")
        const response = await fetch(fetchadress);
        const data = await response.json();
        console.log("success")
        dispatch({ type: 'FETCH_DATA_SUCCESS', payload: data });
      } catch (error) {
        console.log("error")
        dispatch({ type: 'FETCH_DATA_FAILURE', payload: error });
      }
    };
  };