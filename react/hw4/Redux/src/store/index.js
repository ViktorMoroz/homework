import {Provider} from "react-redux";
import list from "./reducers/list";
import {configureStore} from "@reduxjs/toolkit"
import thunk from "redux-thunk";

const store = configureStore({
    reducer: list,
    middleware: [thunk]
}
);

export default function Store(props){
    return (
        <Provider store={store}>
            {props.children}
        </Provider>
    )
}