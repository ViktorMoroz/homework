import React from "react";
import CardList from "../Components/CardList";
import { fetchList } from "../store/reducers/list";
import { connect } from "react-redux";

class Home extends React.Component {
    constructor() {
      super();
      this.state = {
        favOnly: false
      }
    }
    componentDidMount() {
      
      this.props.fetchList("listofgoods.json");
    }
    buttonOnClick() {
      this.setState({ showModal: true });
    }
    render() {
      const { listOfGoods, isLoading, error } = this.props;
      if (isLoading) {
        return <p>Loading...</p>;
      }
  
      if (error) {
        return <p>Error: {error.message}</p>;
      }
      return (
        <main>
         <CardList listOfGoods = {listOfGoods} favOnly = {this.state.favOnly} ></CardList>
        </main>
      );
    }
  }
 
  const mapStateToProps = state => ({
    listOfGoods: state.listOfGoods,
    isLoading: state.isLoading,
    error: state.error
  });
  
  const mapDispatchToProps = {
    fetchList
  };
  export default connect(mapStateToProps, mapDispatchToProps)(Home);