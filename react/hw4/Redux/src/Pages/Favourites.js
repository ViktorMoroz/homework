import React, {useState, useEffect} from 'react';
import CardList from '../Components/CardList';
import { useDispatch, useSelector } from 'react-redux';
import { fetchList } from '../store/reducers/list';
export default function Favourites () {
  const dispatch = useDispatch();
  const listOfGoods = useSelector(state => state.listOfGoods);
  const isLoading = useSelector(state => state.isLoading);
  const error = useSelector(state => state.error);

    const favOnly = true;
    const [shouldRender, setShouldRender] = useState(false);

    const handleChildClick = () => {
      setShouldRender(!shouldRender);
    };
    useEffect(()=>{ 
      dispatch(fetchList("../listofgoods.json"));
    }, [dispatch]);
    if (isLoading) {
      return <p>Loading...</p>;
    }
    if (error) {

      return <p>Error: {error.message}</p>;
    }

    if (listOfGoods.length >= 1) return (

        <main>
          <CardList listOfGoods = {listOfGoods} favOnly = {favOnly} onChildClick={handleChildClick}></CardList>
        </main>

    )

  }

