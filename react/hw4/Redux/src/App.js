import "./App.css";
import React from "react";
import Router from "./Routes/Router";
import Store from "./store"
class App extends React.Component {
  render() {
    return (
      <Store>
        <Router />
      </Store>
    );
  }
}

export default App;