import React from "react";
import {NavLink} from "react-router-dom";
import {Star, Basket} from "../Icons";
function Header (){

    return (
        <header className="header">
          <NavLink className="header-text" aria-current="page" to="/">Головна</NavLink>
          {/* <span>hello</span> */}
          <div className="shopping-basket">
          <NavLink className="nav-link" to="Favourites/">
            <Star fill="gold"></Star>
            <span id="header-fav">{localStorage.favCount>0 && localStorage.favCount}</span>
            </NavLink>
            <NavLink className="nav-link" to="Basket/"><Basket></Basket>
            <span id="shopping-number">{localStorage.shoppingCount>0 && localStorage.shoppingCount}</span>
            </NavLink>
          </div>
          
        </header>
    )
}
export default Header;