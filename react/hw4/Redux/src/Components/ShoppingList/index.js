import React, {useState, useEffect} from 'react';
import ShoppingCard from "../ShoppingCard/";
export default function ShoppingList () {
const [state, setState] = useState({
    shoppingList: false,
    altText: "Loading...",
 });
 const [shouldRender, setShouldRender] = useState(false);
 const handleChildClick = () => {
   setShouldRender(!shouldRender);
 };
useEffect(()=>{
    fetch("../listofgoods.json")
    .then((result) => result.json())
    .then((result) => {
       const listOfGoods = result;
       if(localStorage.shoppingList){
       const shopList = localStorage.shoppingList.split(' ');
       let shoppingList = [];
       shopList.forEach(id => {
        shoppingList.push(listOfGoods.find((element)=>element.id === id));
       });
       setState({shoppingList : shoppingList})}
       else {
        setState({shoppingList : false})
        setState({altText : "Ой, тут пусто"})
       }
       
      }
    )
}, [shouldRender])
    return(<>
        {(state.shoppingList && (<div className="cards">{state.shoppingList.map((card,index)=>{
            return <ShoppingCard card={card} handleChildClick={handleChildClick} key={index}></ShoppingCard>})}</div>)) || <span>{state.altText}</span>}
            </>
    )
}