import React from "react";
import Card from "../Card";
import PropTypes from 'prop-types';
export default class CardList extends React.Component {
    render(){
        let favList
        if(localStorage.favList){
             favList = localStorage.favList.split(' ');}
        return (
            (this.props.favOnly && localStorage.favCount > 0 &&(<div className="cards">{
                favList.map(id => {
                 const card = this.props.listOfGoods.find(element=>element.id === id)
                 return <Card key={card.id} cardInfo={card} onChildClick={this.props.onChildClick}></Card>
            })}</div>)) || (this.props.favOnly && (<div className="cards"><span>Ой, тут пусто</span></div>)) ||
            <div className="cards">{this.props.listOfGoods.map((card)=>{
                return <Card key={card.id} cardInfo={card}></Card>})}</div>
        )
    }
}
CardList.propTypes = {
    listOfGoods: PropTypes.array
}