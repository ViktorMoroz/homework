import React from "react";
import Modal from "../Modal";
class ButtonToShowModal extends React.Component {
    constructor(){
        super();
        this.state = {
          showModal: false,
        }
        this.closeModal = this.closeModal.bind(this)
    }
    closeModal(){
        this.setState({ showModal: false })
    }
    render (){
        const click = this.props.onClick.bind(this);
        
        return(
            <div>
            <button style={{backgroundColor: this.props.backgroundColor}} onClick={()=>click()}>{this.props.text}</button>

            {this.state.showModal && (<Modal modal={this.props.modal} closeModal={this.closeModal}/>)}
            </div>
        )
    }
}
export default ButtonToShowModal;
