import React from "react";
import { ReactComponent as CloseIcon } from "../CloseIcon/CloseIcon.svg";
class Modal extends React.Component {
  constructor() {
    super();
    this.closeModal = this.closeModal.bind(this);
  }

  closeModal(event) {
    if (
      event.target === document.querySelector(".modal") ||
      event.target.className === "close-button" ||
      event.target.id === "close-icon"
    ) {
      return this.props.closeModal();
    }
  }
  render() {
    return (
      <div className="modal" onClick={this.closeModal}>
        <div className="modal-content">
          <header className="header">
            <span className="header-text">{this.props.modal.headerText}</span>
            {this.props.modal.closeButton && (
              <span className="close">
                <CloseIcon></CloseIcon>
              </span>
            )}
          </header>
          <p className="modal-text">{this.props.modal.text}</p>
          <div className="actions">
            {this.props.modal.actions.map((action) => action)}
          </div>
        </div>
      </div>
    );
  }
}
export default Modal;
