import "./App.css";
import React from "react";
import ButtonToShowModal from "./Components/ButtonToShowModal";
class App extends React.Component {
  buttonOnClick(){this.setState({ showModal: true })}
  render() {
    return (
      <main>
        <ButtonToShowModal
          text="Open First Modal"
          backgroundColor="yellow"
          onClick={this.buttonOnClick}
          modal={{
            headerText: "Do you want to delete this file?",
            closeButton: true,
            text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
            actions: [<button className="close-button" key={1}>Ok</button>, <button className="close-button" key={2}>Cancel</button>],
          }}
        ></ButtonToShowModal>
        <ButtonToShowModal
          text="Open Second Modal"
          backgroundColor="orange"
          onClick={this.buttonOnClick}
          modal={{
            headerText: "Pay to continue!",
            closeButton: false,
            text: "In order to continue You have to purchase our useless product for the best price!",
            actions: [<button className="close-button" key={1}>Buy Now</button>, <button className="close-button" key={2}>No</button>],
          }}
        ></ButtonToShowModal>
      </main>
    );
  }
}

export default App;