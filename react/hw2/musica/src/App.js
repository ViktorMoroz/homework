import "./App.css";
import React from "react";
import CardList from "./Components/CardList";
import {ReactComponent as ShoppingBasket} from "./Components/CloseIcon/ShoppingBasket.svg"
import{ReactComponent as Star} from "./Components/CloseIcon/Star.svg"
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      listOfGoods : false,
    }
  }
  componentDidMount() {
    fetch("listofgoods.json")
      .then((result) => result.json())
      .then((result) => this.setState({ listOfGoods : result }))
      if(localStorage.shoppingCount){
      document.querySelector("#shopping-number").innerText=localStorage.shoppingCount;
      }
  }
  buttonOnClick() {
    this.setState({ showModal: true });
  }
  render() {
    return (
      <div>
        <header className="header">
          <span className="header-text">BLYASHANKA - Магазин алюмінієвих банок</span>
          <div className="shopping-basket"><Star fill="gold"></Star>
            <span id="header-fav"></span>
            <ShoppingBasket></ShoppingBasket>
            <span id="shopping-number"></span>
          </div>
        </header>
      <main>
        {this.state.listOfGoods && (<CardList listOfGoods = {this.state.listOfGoods}></CardList>)}
      </main>
      </div>
    );
  }
}

export default App;