import React from "react";
import ButtonToShowModal from "../ButtonToShowModal";
import {ReactComponent as Star} from "../CloseIcon/Star.svg"
export default class Card extends React.Component {
  constructor(){
    super();
    this.state= {
      addToFav: false,
    }
  }
    buttonOnClick() {
        this.setState({ showModal: true });
      }
      favCount () {
        if(Number(localStorage.favCount) > 0){
          document.querySelector("#header-fav").innerText = localStorage.favCount;
          } else {
            document.querySelector("#header-fav").innerText = "";
          }
      }
    favOnClick = ()=>{
      if(localStorage.favList){
        const favList = localStorage.favList.split(' ');
        if(favList.find((element)=>element === this.props.cardInfo.id)){
          const index = favList.indexOf(this.props.cardInfo.id);
          favList.splice(index, 1);
          localStorage.favList= favList.join(" ")
          this.setState({addToFav: false});
          localStorage.favCount=Number(localStorage.favCount)-1;
        }else{
          favList.push(this.props.cardInfo.id);
          localStorage.favList= favList.join(" ")
          this.setState({addToFav: true});
          localStorage.favCount=Number(localStorage.favCount)+1;
        }
      }
      else {
        localStorage.favList=this.props.cardInfo.id;
        localStorage.favCount=1;
        this.setState({addToFav: true});
      }
      this.favCount();
    }
    componentDidMount(){
      if(localStorage.favList){
      const favList = localStorage.favList.split(' '); 
      if(favList.find((element)=>element === this.props.cardInfo.id)){
        this.setState({addToFav: true});
      }
      }
      this.favCount();
    }
    render(){
        return (
            <div className="card">
                <img className="card-image" src={this.props.cardInfo.image} alt="img"/>
                <div className="star-wrapper"><Star fill={(this.state.addToFav && "gold") || "white"} onClick={this.favOnClick}></Star></div>
                <p className="card-name">{this.props.cardInfo.name}</p>
                <p>Ціна: {this.props.cardInfo.price} грн.</p>
                <ButtonToShowModal
          text="Додати у кошик"
          backgroundColor=""
          onClick={this.buttonOnClick}
          modal={{
            headerText: "Додати товар у кошик?",
            closeButton: true,
            text: "Натисніть Так, щоб додати обраний товар у кошик",
            actions: [
              <button className="close-button" key={1} onClick={()=>{
                if(localStorage.shoppingCount){
                  localStorage.shoppingCount=Number(localStorage.shoppingCount)+1;
                  document.querySelector("#shopping-number").innerText=localStorage.shoppingCount;
                } else {
                  localStorage.shoppingCount=1;
                  document.querySelector("#shopping-number").innerText=1;

                }
              }}>
                Так
              </button>,
              <button className="close-button" key={2}>
                Ні
              </button>,
            ],
          }}
        ></ButtonToShowModal>
            </div>
        )
    }
}