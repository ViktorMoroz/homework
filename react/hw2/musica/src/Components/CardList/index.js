import React from "react";
import Card from "../Card";
import PropTypes from 'prop-types';
export default class CardList extends React.Component {
    render(){
        return (
            <div className="cards">{this.props.listOfGoods.map((card)=>{
                return <Card key={card.id} cardInfo={card}></Card>})}</div>
        )
    }
}
CardList.propTypes = {
    listOfGoods: PropTypes.array
}