import React from "react";
import Modal from "../Modal";
import PropTypes from 'prop-types';
class ButtonToShowModal extends React.Component {
    constructor(){
        super();
        this.state = {
          showModal: false,
        }
        this.closeModal = this.closeModal.bind(this)
    }
    closeModal(){
        this.setState({ showModal: false })
    }
    render (){
        const click = this.props.onClick.bind(this);
        
        return(
            <div>
            <button className="button-buy" onClick={()=>click()}>{this.props.text}</button>
            {this.state.showModal && (<Modal modal={this.props.modal} closeModal={this.closeModal}/>)}
            </div>
        )
    }
}

export default ButtonToShowModal;
ButtonToShowModal.propTypes ={
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func,
    modal: PropTypes.object,
};
